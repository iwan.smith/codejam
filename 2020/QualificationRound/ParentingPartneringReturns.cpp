#include <iostream>

#include <string>
#include <vector>
#include <utility>
#include <tuple>

#include <algorithm>
using namespace std;

int main()
{
    
    int T; cin >> T;
    
    for ( int t = 1; t<=T; ++t)
    {
        
        int N; cin >> N;
        
        vector<tuple<int,int,int>> times;
        for( int n = 0; n < N; ++n)
        {
            int start(0), end(0);
            cin >> start; cin >> end;
            
            times.emplace_back(start,end,n);
        }
        
        sort( times.begin(), times.end() );

        string out(N, ' ');
        
        int CBusyUntil(0);
        int JBusyUntil(0);
        for ( const auto& tme: times )
        {
            const int& start = get<0>(tme);
            const int& end   = get<1>(tme);
            const int& index = get<2>(tme);
            if ( start >= CBusyUntil )
            {
                CBusyUntil = end;
                out[index] = 'C';
            }
            else if ( start >= JBusyUntil )
            {            
                JBusyUntil = end;
                out[index] = 'J';
            }
            else
            {
                out = "IMPOSSIBLE";
                break;
            }
            
        }
        
        cout << "Case #" << t << ": " << out << "\n"; 
    }
    
   
}
