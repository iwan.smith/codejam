#include <unordered_set>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
    
    int T; cin >> T;
    
    for ( int t = 1; t<=T; ++t)
    {
        int N; cin >> N;
        
        vector<unordered_set<int>> cols(N);
        vector<unordered_set<int>> rows(N);
        
        
        int trace = 0;
        for ( int i = 0; i < N; ++i)
        {
            for ( int j = 0; j < N; ++j)
            {
                int m; cin >> m;
                if ( i == j ) trace += m;
                
                cols[j].insert(m);
                rows[i].insert(m);
                                
            }
        }
        
        int r(0), c(0);
        for( int i = 0; i < N; ++i)
        {
            r += rows[i].size() < N;
            c += cols[i].size() < N;
        }
        
        cout << "Case #" << t << ": " << trace << " " << r << " " << c << "\n"; 
    }
    
    
}
