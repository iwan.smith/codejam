#include <iostream>
#include <string>

using namespace std;

int main()
{
    
    int T; cin >> T;
    
    for ( int t = 1; t<=T; ++t)
    {

        string in; cin >> in;
        
        string out;
        int tally(0);
        for (const char& c: in)
        {
            int delta =  ( c-'0' ) - tally;
            if ( delta > 0 )
                out += string(delta, '(');
            else
                out += string(-delta, ')');
            out += c;
            tally = c-'0';
        }
        
        out += string(tally, ')');

        
        
        cout << "Case #" << t << ": " << out << "\n"; 
    }
    
    
}
